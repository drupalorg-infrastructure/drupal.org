<?php

/**
 * Put private settings, like $databases, in settings.local.php.
 *
 * Drupal.org-wide settings, like Bakery, go in /var/www/settings.common.php.
 */

$update_free_access = FALSE;
$drupal_hash_salt = '';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

if ( (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')
  || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
  || (isset($_SERVER['HTTP_HTTPS']) && $_SERVER['HTTP_HTTPS'] == 'on')
) {
  $_SERVER['HTTPS'] = 'on';
}

$conf['queue_class_versioncontrol_reposync'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_versioncontrol_reposync'] = [
  'ttr' => 60 * 20, // 20 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_versioncontrol_repomgr'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_versioncontrol_repomgr'] = [
  'ttr' => 60 * 2, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_message_subscribe'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_message_subscribe'] = array(
  'ttr' => 60 * 2, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
);
$conf['queue_class_gitlab_user'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_gitlab_user'] = [
  'ttr' => 60 * 2, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_issue_forks'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_issue_forks'] = [
  'ttr' => 60 * 2, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_in_place_updates'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_in_place_updates'] = [
  'ttr' => 60, // 1 minute.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_release_contents_hashes'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_release_contents_hashes'] = [
  'ttr' => 60 * 5, // 5 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_packaging_pipeline'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_packaging_pipeline'] = [
  'ttr' => 60, // 1 minute.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_commits_comments'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_commits_comments'] = [
  'ttr' => 60, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_keycloak'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_keycloak'] = [
  'ttr' => 60, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];
$conf['queue_class_drupalorg_keycloak_event'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_drupalorg_keycloak_event'] = [
  'ttr' => 60, // 2 minutes.
  'reserve_timeout' => NULL,
  'fork' => TRUE,
];

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

$conf['tracker_batch_size'] = 1000;
$conf['upload_update_batch_size'] = 1000;

$conf['dblog_row_limit'] = 1000000;

// Disable poormanscron.
$conf['cron_safe_threshold'] = 0;

// We don't use link theming.
$conf['theme_link'] = FALSE;

// Turn off node searching, turn on Drupalorg (IRC nickname) searching, and set
// default search to Solr.
$conf['search_active_modules'] = array('user', 'drupalorg', 'apachesolr_search');
$conf['search_default_module'] = 'apachesolr_search';

$conf['apachesolr_site_hash'] = 'e7ikco';

// Release packaging.
$conf['project_release_security_update_tid'] = 100;
$conf['project_release_packager_plugin'] = 'drupalorg';

// Project.
$conf['project_sandbox_numeric_shortname'] = TRUE;
$conf['project_release_sandbox_allow_release'] = FALSE;
$conf['project_allow_machinename_update'] = FALSE;
$conf['project_require_lowercase_machinename'] = TRUE;
// Allow release series branches like '{api}-{major}.' and '{major}.{minor}.'
// to be supported.
$conf['project_release_supportable_branch_pattern'] = '/^(.*\.x-|\d+\.)\d+\.$/';
// For core, allow both '{major}.{minor}.' and for legacy versions, '{major}.'
$conf['project_release_supportable_branch_pattern_project_core'] = '/^(\d+\.\d+\.|[5-7]\.)$/';

$conf['project_issue_cockpit_extra_counts'] = [
  '<abbr title="Reviewed & tested by the community">RTBC</abbr>' => [
    'conditions' => [
      's.field_issue_status_value' => 14,
    ],
    'query' => ['status' => 14],
  ],
];

// drupalorg custom lists module
$conf['lists_security_mail'] = 'security-news@drupal.org';
$conf['lists_security_url'] = 'https://lists.drupal.org/mailman/admin/security-news';
$conf['lists_forum_tids'] = array(
  44 => 'security',
  1852 => 'security',
);

// Drupal.org settings.
$conf['drupalorg_crosssite_lock_permissions'] = TRUE;
$conf['drupalorg_crosssite_trusted_role'] = 36;
$conf['drupalorg_crosssite_community_role'] = 7;
$conf['drupalorg_headless_form_paths'] = array(
  'user/register',
  'user/password',
  'user/login',
  'welcome',
);
// Social link patterns.
$conf['drupalorg_social_link_patterns'] = [
  '%^https?://drupal\.stackexchange\.com/users/[0-9]+%' => 'Drupal Answers',
  '%^https?://([a-z]+\.)?stackexchange\.com/users/[0-9]+%' => 'Stack Exchange',
  '%^https?://([a-z]+\.)?stackoverflow\.com/users/(story/)?[0-9]+%' => 'Stack Overflow',
  '%^https?://([a-z]+\.)?stackoverflow\.com/(cv|story)/[a-z.]+%' => 'Stack Overflow',
  '%^https?://([a-z-]+\.)?(facebook|fb)\.[a-z]+/(#!/)?([^/]+|(pages|people)/[^/]+(/[^/]+)?/[0-9]+|groups/[^/]+)/?(\\?.*)?$%' => 'Facebook',
  '%^https?://(www\.)?github\.com/[^/]+/?$%' => 'GitHub',
  '%^https?://(www\.)?gitlab\.com/[^/]+/?$%' => 'GitLab.com',
  '%^https?://((plus|www)\.)?google\.com/(u/[0-9]/)?(b/)?((\\+|s/)[^/]+|[0-9]+)%' => 'Google',
  '%^https?://profiles\.google\.com/%' => 'Google',
  '%^https?://(www\.)?google\.(com|de)/profiles/%' => 'Google',
  '%^https?://(gratipay|(www\.)?gittip)\.com/[^/]+/?$%' => 'Gratipay',
  '%^https://liberapay\.com/[^/]+/?$%' => 'Liberapay',
  '%^https?://([a-z]+\.)?linkedin\.com/((in|company|companies)/[^/]+(/[a-z]+)?|pub/[^/]+/[^/]+/[^/]+/[^/]+)/?$%' => 'LinkedIn',
  '%^https?://(www\.)?twitter\.com/(#!/)?[^/]+/?$%' => 'Twitter',
  '%^https?://(www\.)?flickr\.com/(people|photos)/[^/]+/?$%' => 'Flickr',
  '%^https?://mastodon\.social/@[^/]+/?%' => 'Mastodon',
  '%^https?://drupal\.community/@[^/]+/?%' => 'Mastodon (drupal.community)',
  '%^https?://bsky.app/profile/[^/]+/?%' => 'Bluesky',
];
// Exclude some pages from Audience Extension.
$conf['drupalorg_perfect_audience_excluded_pages'] = 'user
user/login
user/register
user/password
user/reset/*
project/issues*';

// Version control.
$conf['versioncontrol_repository_plugin_defaults_gitlab_repomgr'] = 'gitlab';
$conf['versioncontrol_repository_plugin_defaults_gitlab_auth_handler'] = 'gitlabaccount';
$conf['versioncontrol_repository_plugin_defaults_gitlab_webviewer_url_handler'] = 'gitlab';

$conf['versioncontrol_project_supported_auth_handlers'] = ['account', 'gitlabaccount'];

$conf['versioncontrol_gitlab_url'] = 'https://git.drupalcode.org';
$conf['versioncontrol_gitlab_url_ssh'] = 'git.drupal.org';
$conf['versioncontrol_gitlab_guzzle_config'] = [
  'timeout' => (drupal_is_cli() || basename($_SERVER['SCRIPT_FILENAME']) === 'runqueue.sh') ? 120.0 : 60.0,
];
$conf['versioncontrol_gitlab_namespace_ids'] = [
  'project' => 2,
  'sandbox' => 3,
  'issue' => 49196,
  'security' => 183118,
];
$conf['versioncontrol_gitlab_user_info'] = [
  'projects_limit' => 0,
];
$conf['versioncontrol_gitlab_user_picture_style'] = 'grid-2-2x-square';

// For VersioncontrolRepositoryUrlHandlerGitlab.
$conf['versioncontrol_repository_gitlab_base_url_gitlab'] = 'https://git.drupalcode.org';

$conf['drupalorg_git_reserved_names'] = [
  // Gitlab reserved names.
  '.well-known', 'admin', 'all', 'assets', 'ci', 'dashboard', 'files',
  'groups', 'help', 'hooks', 'issues', 'merge_requests', 'new', 'notes',
  'profile', 'projects', 'public', 'repository', 'robots.txt', 's', 'search',
  'services', 'snippets', 'teams', 'u', 'unsubscribes', 'users',
  // 🤖, 👻 & 🛥
  'drupalbot', 'ghost', 'tugboat', 'root-admin', 'gitpod',
  // Namespaces.
  'project', 'sandbox', 'issue', 'security',
];
$conf['drupalorg_project_reserved_names'] = [
  'webmasters', // https://www.drupal.org/project/site_moderators/issues/3173198
  // Would collide with packages at
  // https://packagist.org/drupal?query=drupal%2F. Dashes are not allowed in
  // project names, so names like “core-dev” do not need to be reserved.
  'core', 'drupalctl', 'entity_views', 'predis', 'tangler', 'dev', 'recommended',
  // Core components that do not already have projects with the same name. Need
  // project_composer issue to coordinate namespace handling before removal.
  'announcements_feed', 'automated_cron', 'ban', 'block_content', 'comment',
  'content_translation', 'datetime_range', 'dblog', 'demo_umami_content',
  'dynamic_page_cache', 'field_sql_storage', 'field_ui', 'help_topics',
  'inline_form_errors', 'language', 'layout_discovery', 'menu',
  'menu_link_content', 'menu_ui', 'migrate_drupal_ui', 'mysql', 'number',
  'overlay', 'page_cache', 'path_alias', 'pgsql', 'search', 'serialization',
  'settings_tray', 'sqlite', 'stable9', 'starterkit_theme', 'syslog', 'text',
  'update', 'user', 'views_ui', 'workflows', 'workspaces',
  // Composer reserved names
  // https://github.com/composer/composer/blob/9a656854adf77fb984bb806986d493bba7f673fd/src/Composer/Package/Loader/ValidatingArrayLoader.php#L463\
  'nul', 'con', 'prn', 'aux', 'com1', 'com2', 'com3', 'com4', 'com5', 'com6',
  'com7', 'com8', 'com9', 'lpt1', 'lpt2', 'lpt3', 'lpt4', 'lpt5', 'lpt6',
  'lpt7', 'lpt8', 'lpt9',
];

$conf['drupalorg_packagist_reserved_names'] = [
  'at_base', 'coder', 'commerce_taxcloud', 'console', 'console-ar',
  'console-ca', 'console-core', 'console-develop', 'console-dotenv',
  'console-en', 'console-es', 'console-extend', 'console-extend-example',
  'console-extend-plugin', 'console-fa-ir', 'console-fr', 'console-he',
  'console-hi', 'console-hu', 'console-id', 'console-ja', 'console-ko',
  'console-launcher', 'console-mr', 'console-pa', 'console-pt-br',
  'console-ro', 'console-ru', 'console-ur', 'console-vn', 'console-yaml',
  'console-zh-hans', 'console-zh-hant', 'cors', 'currency', 'd7-plugin',
  'default_content', 'dev-dependencies', 'drupal', 'drupal-builder',
  'drupal-core-strict', 'drupal-driver', 'drupal-extension',
  'drupal-library-installer-plugin', 'drupal-scaffold', 'drupalctl',
  'dynamic_entity_reference', 'entity_views', 'legacy-project',
  'legacy-scaffold-assets', 'marconi', 'micro', 'opcache', 'openstack_queues',
  'panopoly-framework', 'parse-composer', 'payment', 'php-signify',
  'pinned-dev-dependencies', 'plugin_selector', 'predis',
  'recommended-project', 'root', 'search_api_elasticsearch',
  'spaces_og_accelerated', 'stringoverrides', 'tangler', 'tqextension',
];

$conf['drupalorg_versioncontrol_vcs'] = 'gitlab';
$conf['drupalorg_versioncontrol_plugins'] = [
  'auth_handler' => 'gitlabaccount', // We can't rely on the $conf default for this b/c vc_project doesn't respect it.
  'committer_mapper' => 'drupalorg_mapper',
];

$conf['drupalorg_projects_in_core'] = [
  3196355, // a11y_autocomplete
  3195030, // once
];

$conf['tugboat_gitlab_user_id'] = 53685;

// Allow local images from domains that have served Drupal.org. See
// https://drupal.org/node/1737022 & https://drupal.org/node/2257291
$conf['filter_html_image_secure_domains'] = array(
  'drupal.org',
  'www.drupal.org',
);

// Do not let metatag run amok. See https://drupal.org/node/2233753.
$conf['metatag_load_all_pages'] = FALSE;

// Prevent https://drupal.org/node/2284483 from happening again.
$conf['pathauto_node_project_core_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_issue_pattern'] = 'project/[node:field-project:field_project_machine_name]/issues/[node:nid]';
$conf['pathauto_node_project_core_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_distribution_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_distribution_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_drupalorg_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_drupalorg_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_module_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_module_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_engine_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_engine_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_translation_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_translation_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
// Path auto pattern for organization nodes
$conf['pathauto_node_organization_pattern'] = '[node:title]';
// Path auto pattern for forum
$conf['pathauto_forum_pattern'] = 'forum/[term:root:name]/[term:name]';
// Path auto node forum pattern
$conf['pathauto_node_forum_pattern'] = '[node:taxonomy-forums:url:path]/[node:created:custom:Y-m-d]/[node:title]';
// Path auto pattern for case studies
$conf['pathauto_node_casestudy_pattern'] = 'case-study/[node:title]';
// Path auto pattern for security advisories.
$conf['pathauto_node_sa_pattern'] = '[node:drupalorg-sa-id]';
// Path auto pattern for sections
$conf['pathauto_node_section_pattern'] = '[node:field-parent-section:url:path]/[node:title]';
// Path auto pattern for pages
//$conf['pathauto_node_page_pattern'] = '[node:og-group-ref:url:path]/[node:title]';

$conf['project_release_download_base_project_release'] = 'https://ftp.drupal.org/';

// Composer Manager
$conf['composer_manager_vendor_dir'] = 'sites/all/vendor';
$conf['composer_manager_file_dir'] = 'sites/default/composer';
$conf['composer_manager_autobuild_file'] = 0;
$conf['composer_manager_autobuild_packages'] = 0;

// Message stack
$conf['message_subscribe_use_queue'] = TRUE;
// Extend project issue’s flag for all following.
$conf['message_subscribe_flag_prefix'] = 'project';

// Make Features tolerable with lots of menu links.
$conf['features_admin_menu_links_menus'] = [];

/**
 * CLI
 */
if (php_sapi_name() === 'cli') {
  ini_set('memory_limit', '1000M');
}

/**
 * todo Variable overrides from D6. Exctract and comment when used. Delete when
 * confirmed unused.
 */
/*
$conf = array(
  // Solr
  'apachesolr_optimize_interval' => 0,
  'search_cron_limit' => 1000,

  // Project
  'project_issue_invalid_releases' => array(94702),
  'project_issue_global_subscribe_page' => FALSE,
  'project_usage_show_weeks' => 4,
  'project_browse_nodes' => 200,
  'project_issue_hook_cron' => FALSE,
);
*/

/**
 * Include a common settings file if it exists.
 */
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}

// Longer sessions for www.drupal.org.
ini_set('session.gc_maxlifetime', 2462400); // 28.5 days
ini_set('session.cookie_lifetime', 2462400);

// Marketplace ordering.
$conf['drupalorg_org_credit_weights'] = [
  'issue credit' => 1.0,
  'issue credit project divisor' => 1.0,
  'issue credit project usage override' => [
    // Project node ID => synthetic usage
  ],
  'supporting partner' => [
    'hosting' => 0.0,
    'hosting_premium' => 0.0,
    'hosting_signature' => 0.0,
    'community_supporter' => 0.0,
    'supporting' => 0.0,
    'supporting_premium' => 0.0,
    'supporting_signature' => 0.0,
    'supporting_enterprise' => 0.0,
    'technology' => 0.0,
    'technology_community' => 0.0,
    'technology_premium' => 0.0,
    'technology_signature' => 0.0,
    'technology_enterprise' => 0.0,
    'supporting_customer' => 0.0,
    'supporting_customer_charter' => 0.0,
    'drupalcares_2020_premiere' => 0.0,
    'drupalcares_2020_champion' => 0.0,
    'drupalcares_2020' => 0.0,
  ],
  'supporting partner market' => [],
  'membership' => 0.0,
  'membership_individual' => 0.0,
  'd8 case study' => 0.0,
  'd9 case study' => 0.0,
  'd10 case study' => 0.0,
  'project supported' => 0.0,
  'contribution role' => [
    'base' => 1.0,
    'time' => [
      'expired' => 1,
      'current' => 1,
    ],
    'time duration' => [
      'null' => 0,
      'N/A' => 0,
      '20 minutes' => 1,
      '1 hour' => 1,
      'several hours' => 1,
      'several days' => 1,
      'several weeks' => 1,
      'ongoing' => 1,
    ],
    'time monthly' => [
      'null' => 0,
      'N/A' => 0,
      '1 hour' => 1,
      '2-4 hours' => 1,
      '5-10 hours' => 1,
      'More than 10 hours' => 1,
    ],
    'contrib area' => [
      'null' => 0,
      'Accessibility' => 1,
      'Community building' => 1,
      'Contributed modules, themes, and distributions' => 1,
      'Contributor guide' => 1,
      'Documentation' => 1,
      'Drupal core' => 1,
      'Drupal.org websites' => 1,
      'Event planning' => 1,
      'Knowledge sharing' => 1,
      'Marketing' => 1,
      'Mentoring' => 1,
      'Support' => 1,
      'Translation' => 1,
      'Usability' => 1,
    ],
  ]
];

$marketplace_settings = '/usr/local/www-marketplace-rank/settings.www-marketplace.php';
if (file_exists($marketplace_settings)) {
  include $marketplace_settings;
}

// 🇪🇺
$conf['drupalorg_crosssite_gdpr_copy'] = [
  'message' => 'Can we use first and third party cookies and web beacons to <a href="https://www.drupal.org/terms">understand our audience, and to tailor promotions you see</a>?',
  'yes' => 'Yes, please',
  'no' => 'No, do not track me',
  'status' => 'You are currently <strong class="opted-in">opted in</strong><strong class="opted-out">opted out</strong>.',
];

// Hide some interest groups on the user form to keep the form under control.
$conf['drupalorg_suppress_interest_groups'] = [
  'de188429fc', // How would you best describe your business?
  'ef73b36223', // What is your role in your organization?
];

/**
 * Include a local settings file if it exists.
 */
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}

if (isset($register_cookie_domain)) {
  if (strpos($_SERVER['HTTP_HOST'], 'register.') === 0) {
    // Use cookie domain that both register.… & www.… can use.
    $cookie_domain = $register_cookie_domain;
  }
  else {
    $register_name = 'SSESS' . substr(hash('sha256', $register_cookie_domain), 0, 32);
    if (isset($_COOKIE[$register_name])) {
      // Change session cookie to use more-strict cookie domain.
      $params = session_get_cookie_params();
      $expire = $params['lifetime'] ? REQUEST_TIME + $params['lifetime'] : 0;
      $usual_name = 'SSESS' . substr(hash('sha256', $cookie_domain), 0, 32);
      setcookie($usual_name, $_COOKIE[$register_name], $expire, $params['path'], $cookie_domain, TRUE, $params['httponly']);
      // Make cookie available for session handling.
      $_COOKIE[$usual_name] = $_COOKIE[$register_name];
      // Remove old cookie.
      setcookie($register_name, '', REQUEST_TIME - 3600, $params['path'], $register_cookie_domain, TRUE, $params['httponly']);
    }
  }
}

// runqueue.sh does not like the possibility of caching. The GitLab webhook is
// invoked with a GET request, don’t let caching near it.
if (basename($_SERVER['SCRIPT_FILENAME']) === 'runqueue.sh' || (isset($_SERVER['SCRIPT_URL']) && $_SERVER['SCRIPT_URL'] === '/drupalorg-issue-fork-merge-request-event')) {
  $conf['cache'] = FALSE;
}
