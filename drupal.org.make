core = 7.x
api = 2
projects[drupal][version] = "7.103"


; Core hacks :(

; Forums performance improvements
; https://www.drupal.org/project/drupal/issues/145353#comment-12578333
projects[drupal][patch][] = "https://www.drupal.org/files/issues/2018-04-18/145353-d7.diff"

; Canceling a user account, assigning content to Anonymous, should clear the comment name
; https://www.drupal.org/node/2472043#comment-9849229
projects[drupal][patch][] = "https://www.drupal.org/files/issues/canceling_a_user-2472043-17.patch"

; SchemaCache is prone to persistent cache corruption
; https://www.drupal.org/node/2450533#comment-11000623
projects[drupal][patch][] = "https://www.drupal.org/files/issues/2450533.diff"

; Prevent post-dated posts from staying at the top of aggregated feeds
; https://www.drupal.org/project/drupal/issues/1420718#comment-12624936
projects[drupal][patch][] = "https://www.drupal.org/files/issues/2018-05-22/1420718-d7.patch"

; Allow form tokens to be used on anonymous forms in some cases
; https://www.drupal.org/project/drupal/issues/1803712#comment-13225053
projects[drupal][patch][] = "https://www.drupal.org/files/issues/2019-08-16/1803712.patch"

; Set fixed "from:" and add "Reply-to:" to comply with DMARC
; https://www.drupal.org/project/drupal/issues/111702#comment-15533511
projects[drupal][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/drupal-217.diff"


; Modules
projects[addressfield][version] = "1.3"

projects[advagg][version] = "2.33"

projects[aggregator_item_length][version] = "1.1"

projects[akismet][version] = "2.x-dev"
projects[akismet][download][revision] = "03c0e8a"
; Improve $access_permissions array handling in _akismet_actions_access_callbacks()
; https://www.drupal.org/project/2948481/issues/2680597#comment-12604499
projects[akismet][patch][] = "https://www.drupal.org/files/issues/2018-05-07/2680597-6.diff"
; "Report node to Akismet as spam and unpublish" action should properly unpublish
; https://www.drupal.org/project/akismet/issues/2590101#comment-13193699
projects[akismet][patch][] = "https://www.drupal.org/files/issues/2019-07-23/2590101.patch"

projects[apachesolr][version] = "1.8"

projects[apachesolr_multisitesearch][version] = "1.1"

projects[asciidoc_display][version] = "1.0-beta3"

projects[beanstalkd][version] = "1.x-dev"
; claimItem() "$lease_time" argument is incorrect
; https://www.drupal.org/node/2496115#comment-10139662
projects[beanstalkd][download][revision] = "716344f"

projects[better_exposed_filters][version] = "3.6"

projects[bueditor][version] = "1.8"

projects[bueditor_plus][version] = "1.4"

projects[captcha][version] = "1.7"

projects[ckeditor_lts][version] = "1.25"

projects[codefilter][version] = "1.x-dev"
; When using prism, the pre wrappers get infinitely wrapped on ckeditor
; https://www.drupal.org/node/2704949
projects[codefilter][download][revision] = "8c96e67"
; < within <?php is not escaped for codefilter_prism
; https://www.drupal.org/node/2725111#comment-12272149
projects[codefilter][patch][] = "https://www.drupal.org/files/issues/2725111-18.patch"

projects[comment_fragment][version] = "1.2"

projects[conflict][version] = "1.1-rc1"

projects[ctools][version] = "1.15"

projects[d3][version] = "1.0-alpha3"

projects[d3_sparkline][version] = "1.x-dev"
projects[d3_sparkline][download][revision] = "6897ca0"

projects[date][version] ="2.11-beta3"

projects[dereference_list][version] = "1.x-dev"
projects[dereference_list][download][revision] = "4acf25d"
; Handle deletion of referenced items
; https://www.drupal.org/project/dereference_list/issues/1569864#comment-12732517
projects[dereference_list][patch][] = "https://www.drupal.org/files/issues/2018-08-17/1569864.patch"

projects[diff][version] = "3.2"

projects[entity][version] = "1.9"

projects[entityreference][version] = "1.5"
; Entity reference list View Pager: Items to display is broken
; https://www.drupal.org/project/entityreference/issues/1700112#comment-12224334
projects[entityreference][patch][] = "https://www.drupal.org/files/issues/entity_reference_list-1700112-21.patch"

projects[entityreference_prepopulate][version] = "1.7"

projects[extended_file_field][version] = "1.x-dev"
projects[extended_file_field][download][revision] = "06b8af4"

projects[facetapi][version] = "1.x-dev"
; Notice: Undefined offset: 1 in FacetapiAdapter->processActiveItems() line 312
; https://www.drupal.org/node/2378693
projects[facetapi][download][revision] = "0e306dc"

projects[fasttoggle][version] = "1.7"

projects[features][version] = "2.13"

projects[feeds][version] = "2.0-beta6"

projects[field_collection][version] = "1.1"
projects[field_collection][patch][] = "https://git.drupalcode.org/project/field_collection/-/commit/c8eea2ef240df3b0a4a9cf126e429b3217331881.diff"

projects[field_extrawidgets][version] = "1.1"

projects[field_formatter_settings][version] = "1.1"

projects[field_group][version] = "1.6"
; after update from 1.5 to 1.6 empty field groups (because of field permissions) are now being displayed as empty groups
; https://www.drupal.org/project/field_group/issues/2926605#comment-12842451
projects[field_group][patch][] = "https://www.drupal.org/files/issues/2018-11-04/2926605_0.patch"

projects[fieldable_panels_panes][version] = "1.10"

projects[filter_html_image_secure][version] = "1.1-beta1"

projects[flag][version] = "3.9"

projects[flag_tracker][version] = "2.2"

projects[geocoder][version] = "1.x-dev"
projects[geocoder][download][revision] = "6bc356a5"
; OSM Nominatim does not work
; https://www.drupal.org/project/geocoder/issues/3050679#comment-13271579
projects[geocoder][patch][] = "https://www.drupal.org/files/issues/2019-09-25/fix_osm_nominatim_cumulates_2682613_2682527-3050679-6.patch"
; Geofield remains empty when I choose "Geocode from another field"
; https://www.drupal.org/project/geocoder/issues/3002517#comment-13530859
projects[geocoder][patch][] = "https://www.drupal.org/files/issues/2020-03-30/geocoder-geofield-remains-empty-3002517-134.patch"

projects[geofield][version] = "2.4"

projects[geophp][version] = "1.7"

#projects[google_tag][version] = "2.4"
projects[google_tag][download][type] = "get"
projects[google_tag][download][url] = "https://releases.tag1.com/google_tag-7.x-2.4-9IYksnnrJLigvzXP.tar.gz"

projects[homebox][version] = "3.x-dev"
projects[homebox][download][revision] = "b4f93df"

projects[honeypot][version] = "1.26"

projects[httpbl][version] = "1.0"

projects[job_scheduler][version] = "2.0"

projects[openid_connect][version] = "1.3"
; Support to logout from the OAuth2 Server provider
; https://www.drupal.org/project/openid_connect/issues/2882270
projects[openid_connect][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/openid_connect-107.diff"
; Add optional username and email synchronization from identity provider
; https://www.drupal.org/project/openid_connect/issues/3452530
projects[openid_connect][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/openid_connect-111.diff"
; Trying to get property of non-object in locale_language_url_rewrite_url()
; https://www.drupal.org/project/openid_connect/issues/3464573
projects[openid_connect][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/openid_connect-119.diff"

projects[leaflet][version] = "1.x-dev"
projects[leaflet][download][revision] = "908f50b"
; Clean up AdvAgg L.Icon.Default.imagePath setting
; https://www.drupal.org/project/leaflet/issues/3130383#comment-13569177
projects[leaflet][patch][] = "https://www.drupal.org/files/issues/2020-04-23/3130383.patch"

projects[libraries][version] = "2.3"

projects[link][version] = "1.11"

projects[machine_name][version] = "1.x-dev"
projects[machine_name][download][revision] = "94b7446"

projects[mailchimp][version] = "5.x-dev"
; Warning: Invalid argument supplied for foreach() in mailchimp_update_local_cache()
; https://www.drupal.org/project/mailchimp/issues/2857646#comment-13272303
projects[mailchimp][download][revision] = "38cc417"

projects[message][version] = "1.12"

projects[message_notify][version] = "2.5"

projects[message_subscribe][version] = "1.x-dev"
; By default, don't notify blocked users
; https://www.drupal.org/node/2184567
projects[message_subscribe][download][revision] = "34c74c9"

projects[message_follow][version] = "1.2"

projects[metatag][version] = "1.27"

projects[nodechanges][version] = "1.x-dev"
projects[nodechanges][download][revision] = "ee340f8"

projects[og][version] = "2.9"
; og_ui should give users the theme, not admin ui when creating groups
; https://www.drupal.org/node/1800208#comment-6545144
projects[og][patch][] = "http://drupal.org/files/og_ui-group_node_add_theme-1800208-5.patch"

projects[og_menu][version] = "3.3"

projects[panelizer][version] = "3.4"
; Better Revision Handling
; https://www.drupal.org/node/2457113#comment-11521887
projects[panelizer][patch][2457113] = "https://www.drupal.org/files/issues/panelizer-n2457113-69.patch"
; Add language support
; https://www.drupal.org/node/2070891#comment-11995941
projects[panelizer][patch][2070891] = "https://www.drupal.org/files/issues/2070891-26-.patch"

projects[panels][version] = "3.9"
; Fix IPE JS alert (Panelizer is Incompatible with Moderation)
; http://drupal.org/node/1402860#comment-9729091
projects[panels][patch][1402860] = "https://www.drupal.org/files/issues/panelizer_is-1402860-82-fix-ipe-end-js-alert.patch"

projects[pathauto][version] = "1.3"

projects[project][version] = "2.x-dev"
projects[project][download][revision] = "b8bedd4"

projects[project_composer][version] = "1.x-dev"
projects[project_composer][download][revision] = "6df7e5b"

projects[project_git_instructions][version] = "1.x-dev"
projects[project_git_instructions][download][revision] = "01f6973"

projects[project_issue][version] = "2.x-dev"
projects[project_issue][download][revision] = "dfb2f0a"

projects[project_solr][version] = "1.x-dev"
projects[project_solr][download][revision] = "86a84fa"

projects[r4032login][version] = "1.8"

projects[recaptcha][version] = "2.3"

projects[redirect][version] = "1.0-rc4"

projects[restws][version] = "2.10"

projects[role_activity][version] = "3.0-alpha2"

projects[salesforce][version] = "3.3"
;This patch does not appear to have been committed to the main branch of the Drupal 7 Salesforce module.
;The reason for this appears to be the fact that if a mapping is created without setting one of the fields as the upsert ‘Key’ then issues may occur.
;Patch issue: https://www.drupal.org/project/salesforce/issues/2620344#comment-12171124
projects[salesforce][patch][] = "https://www.drupal.org/files/issues/salesforce-n2620344-9.patch"
; Error deleting SF connection in the salesforce_activity tab
; https://www.drupal.org/project/salesforce/issues/3089541#comment-13321108
projects[salesforce][patch][] = "https://www.drupal.org/files/issues/2019-10-22/sf_activity_delete_error_3089541-2.patch"

projects[sampler][version] = "1.1-beta1"

projects[search_api][version] = "1.18"

projects[search_api_db][version] = "1.5"
; Do not re-tokenize text
; https://www.drupal.org/node/2201041#comment-9174517
projects[search_api_db][patch][] = "https://www.drupal.org/files/issues/2201041-drupal.org-do-not-test.patch"

projects[signature_permissions][version] = "1.3"

projects[strongarm][version] = "2.0"

projects[table_of_contents][version] = "2.x-dev"
projects[table_of_contents][download][revision] = "7fd476e"

projects[tag1_d7es][version] = "1.2"

projects[token][version] = "1.7"

projects[token_formatters][version] = "1.2"

projects[url][version] = "1.0"

projects[user_restrictions][version] = "1.1"

projects[versioncontrol][version] = "1.x-dev"
projects[versioncontrol][download][revision] = "b1061fd"

projects[versioncontrol_git][version] = "1.x-dev"
projects[versioncontrol_git][download][revision] = "4cd03a9"

projects[versioncontrol_gitlab][version] = "1.x-dev"
projects[versioncontrol_gitlab][download][revision] = "914f678"

projects[versioncontrol_project][version] = "1.x-dev"
projects[versioncontrol_project][download][revision] = "424990d"

projects[views][version] = "3.21"

projects[views_bulk_operations][version] = "3.5"
; Default action behaviors in getAccessMask()
; https://www.drupal.org/node/2254871#comment-10464355
projects[views_bulk_operations][patch][] = "https://www.drupal.org/files/issues/2254871.diff"

projects[views_content_cache][version] = "3.0-alpha3"

projects[views_data_export] = "3.2"

projects[views_field_view][version] = "1.2"

projects[views_litepager][version] = "3.0"

projects[views_selective_filters][version] = "1.6"


; Custom modules
projects[drupalorg][version] = "3.x-dev"
projects[drupalorg][download][revision] = "a58aa689"


; Libraries
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.25.0-lts/ckeditor_4.25.0-lts_basic.zip"

libraries[ckeditor_ajax][download][type] = "get"
libraries[ckeditor_ajax][download][url] = "https://download.ckeditor.com/ajax/releases/ajax_4.25.0-lts.zip"
libraries[ckeditor_ajax][type] = "libraries"
libraries[ckeditor_ajax][subdir] = "ckeditor/plugins"
libraries[ckeditor_ajax][directory_name] = "ajax"

libraries[ckeditor_blockquote][download][type] = "get"
libraries[ckeditor_blockquote][download][url] = "https://download.ckeditor.com/blockquote/releases/blockquote_4.25.0-lts.zip"
libraries[ckeditor_blockquote][type] = "libraries"
libraries[ckeditor_blockquote][subdir] = "ckeditor/plugins"
libraries[ckeditor_blockquote][directory_name] = "blockquote"

libraries[ckeditor_button][download][type] = "get"
libraries[ckeditor_button][download][url] = "https://download.ckeditor.com/button/releases/button_4.25.0-lts.zip"
libraries[ckeditor_button][type] = "libraries"
libraries[ckeditor_button][subdir] = "ckeditor/plugins"
libraries[ckeditor_button][directory_name] = "button"

libraries[ckeditor_codestyle][download][type] = "get"
libraries[ckeditor_codestyle][download][url] = "https://github.com/japerry/ckeditor_codestyle/archive/0.2.tar.gz"
libraries[ckeditor_codestyle][type] = "libraries"
libraries[ckeditor_codestyle][subdir] = "ckeditor/plugins"
libraries[ckeditor_codestyle][directory_name] = "codestyle"

libraries[ckeditor_codesnippet][download][type] = "get"
libraries[ckeditor_codesnippet][download][url] = "https://download.ckeditor.com/codesnippet/releases/codesnippet_4.25.0-lts.zip"
libraries[ckeditor_codesnippet][type] = "libraries"
libraries[ckeditor_codesnippet][subdir] = "ckeditor/plugins"
libraries[ckeditor_codesnippet][directory_name] = "codesnippet"

libraries[ckeditor_filetools][download][type] = "get"
libraries[ckeditor_filetools][download][url] = "https://download.ckeditor.com/filetools/releases/filetools_4.25.0-lts.zip"
libraries[ckeditor_filetools][type] = "libraries"
libraries[ckeditor_filetools][subdir] = "ckeditor/plugins"
libraries[ckeditor_filetools][directory_name] = "filetools"

libraries[ckeditor_floatpanel][download][type] = "get"
libraries[ckeditor_floatpanel][download][url] = "https://download.ckeditor.com/floatpanel/releases/floatpanel_4.25.0-lts.zip"
libraries[ckeditor_floatpanel][type] = "libraries"
libraries[ckeditor_floatpanel][subdir] = "ckeditor/plugins"
libraries[ckeditor_floatpanel][directory_name] = "floatpanel"

libraries[ckeditor_format][download][type] = "get"
libraries[ckeditor_format][download][url] = "https://download.ckeditor.com/format/releases/format_4.25.0-lts.zip"
libraries[ckeditor_format][type] = "libraries"
libraries[ckeditor_format][subdir] = "ckeditor/plugins"
libraries[ckeditor_format][directory_name] = "format"

libraries[ckeditor_horizontalrule][download][type] = "get"
libraries[ckeditor_horizontalrule][download][url] = "https://download.ckeditor.com/horizontalrule/releases/horizontalrule_4.25.0-lts.zip"
libraries[ckeditor_horizontalrule][type] = "libraries"
libraries[ckeditor_horizontalrule][subdir] = "ckeditor/plugins"
libraries[ckeditor_horizontalrule][directory_name] = "horizontalrule"

libraries[ckeditor_htmlwriter][download][type] = "get"
libraries[ckeditor_htmlwriter][download][url] = "https://download.ckeditor.com/htmlwriter/releases/htmlwriter_4.25.0-lts.zip"
libraries[ckeditor_htmlwriter][type] = "libraries"
libraries[ckeditor_htmlwriter][subdir] = "ckeditor/plugins"
libraries[ckeditor_htmlwriter][directory_name] = "htmlwriter"

libraries[ckeditor_image2][download][type] = "get"
libraries[ckeditor_image2][download][url] = "https://download.ckeditor.com/image2/releases/image2_4.25.0-lts.zip"
libraries[ckeditor_image2][type] = "libraries"
libraries[ckeditor_image2][subdir] = "ckeditor/plugins"
libraries[ckeditor_image2][directory_name] = "image2"

libraries[ckeditor_lineutils][download][type] = "get"
libraries[ckeditor_lineutils][download][url] = "https://download.ckeditor.com/lineutils/releases/lineutils_4.25.0-lts.zip"
libraries[ckeditor_lineutils][type] = "libraries"
libraries[ckeditor_lineutils][subdir] = "ckeditor/plugins"
libraries[ckeditor_lineutils][directory_name] = "lineutils"

libraries[ckeditor_listblock][download][type] = "get"
libraries[ckeditor_listblock][download][url] = "https://download.ckeditor.com/listblock/releases/listblock_4.25.0-lts.zip"
libraries[ckeditor_listblock][type] = "libraries"
libraries[ckeditor_listblock][subdir] = "ckeditor/plugins"
libraries[ckeditor_listblock][directory_name] = "listblock"

libraries[ckeditor_magicline][download][type] = "get"
libraries[ckeditor_magicline][download][url] = "https://download.ckeditor.com/magicline/releases/magicline_4.25.0-lts.zip"
libraries[ckeditor_magicline][type] = "libraries"
libraries[ckeditor_magicline][subdir] = "ckeditor/plugins"
libraries[ckeditor_magicline][directory_name] = "magicline"

libraries[ckeditor_notification][download][type] = "get"
libraries[ckeditor_notification][download][url] = "https://download.ckeditor.com/notification/releases/notification_4.25.0-lts.zip"
libraries[ckeditor_notification][type] = "libraries"
libraries[ckeditor_notification][subdir] = "ckeditor/plugins"
libraries[ckeditor_notification][directory_name] = "notification"

libraries[ckeditor_notificationaggregator][download][type] = "get"
libraries[ckeditor_notificationaggregator][download][url] = "https://download.ckeditor.com/notificationaggregator/releases/notificationaggregator_4.25.0-lts.zip"
libraries[ckeditor_notificationaggregator][type] = "libraries"
libraries[ckeditor_notificationaggregator][subdir] = "ckeditor/plugins"
libraries[ckeditor_notificationaggregator][directory_name] = "notificationaggregator"

libraries[ckeditor_panel][download][type] = "get"
libraries[ckeditor_panel][download][url] = "https://download.ckeditor.com/panel/releases/panel_4.25.0-lts.zip"
libraries[ckeditor_panel][type] = "libraries"
libraries[ckeditor_panel][subdir] = "ckeditor/plugins"
libraries[ckeditor_panel][directory_name] = "panel"

; Prism for ckeditor. Forked from https://github.com/ranelpadon/ckeditor-prism
libraries[ckeditor_prism][download][type] = "get"
libraries[ckeditor_prism][download][url] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/get/1af9ec7f8967.zip"
libraries[ckeditor_prism][download][subtree] = "drupalorg-infrastructure-drupal.org-sites-common-1af9ec7f8967/prism-ckeditor"
libraries[ckeditor_prism][type] = "libraries"
libraries[ckeditor_prism][subdir] = "ckeditor/plugins"
libraries[ckeditor_prism][directory_name] = "prism"

libraries[ckeditor_resize][download][type] = "get"
libraries[ckeditor_resize][download][url] = "https://download.ckeditor.com/resize/releases/resize_4.25.0-lts.zip"
libraries[ckeditor_resize][type] = "libraries"
libraries[ckeditor_resize][subdir] = "ckeditor/plugins"
libraries[ckeditor_resize][directory_name] = "resize"

libraries[ckeditor_richcombo][download][type] = "get"
libraries[ckeditor_richcombo][download][url] = "https://download.ckeditor.com/richcombo/releases/richcombo_4.25.0-lts.zip"
libraries[ckeditor_richcombo][type] = "libraries"
libraries[ckeditor_richcombo][subdir] = "ckeditor/plugins"
libraries[ckeditor_richcombo][directory_name] = "richcombo"

libraries[ckeditor_table][download][type] = "get"
libraries[ckeditor_table][download][url] = "https://download.ckeditor.com/table/releases/table_4.25.0-lts.zip"
libraries[ckeditor_table][type] = "libraries"
libraries[ckeditor_table][subdir] = "ckeditor/plugins"
libraries[ckeditor_table][directory_name] = "table"

libraries[ckeditor_templates][download][type] = "get"
libraries[ckeditor_templates][download][url] = "https://download.ckeditor.com/templates/releases/templates_4.25.0-lts.zip"
libraries[ckeditor_templates][type] = "libraries"
libraries[ckeditor_templates][subdir] = "ckeditor/plugins"
libraries[ckeditor_templates][directory_name] = "templates"

libraries[ckeditor_toolbar][download][type] = "get"
libraries[ckeditor_toolbar][download][url] = "https://download.ckeditor.com/toolbar/releases/toolbar_4.25.0-lts.zip"
libraries[ckeditor_toolbar][type] = "libraries"
libraries[ckeditor_toolbar][subdir] = "ckeditor/plugins"
libraries[ckeditor_toolbar][directory_name] = "toolbar"

libraries[ckeditor_widget][download][type] = "get"
libraries[ckeditor_widget][download][url] = "https://download.ckeditor.com/widget/releases/widget_4.25.0-lts.zip"
libraries[ckeditor_widget][type] = "libraries"
libraries[ckeditor_widget][subdir] = "ckeditor/plugins"
libraries[ckeditor_widget][directory_name] = "widget"

libraries[ckeditor_widgetselection][download][type] = "get"
libraries[ckeditor_widgetselection][download][url] = "https://download.ckeditor.com/widgetselection/releases/widgetselection_4.25.0-lts.zip"
libraries[ckeditor_widgetselection][type] = "libraries"
libraries[ckeditor_widgetselection][subdir] = "ckeditor/plugins"
libraries[ckeditor_widgetselection][directory_name] = "widgetselection"

libraries[ckeditor_xml][download][type] = "get"
libraries[ckeditor_xml][download][url] = "https://download.ckeditor.com/xml/releases/xml_4.25.0-lts.zip"
libraries[ckeditor_xml][type] = "libraries"
libraries[ckeditor_xml][subdir] = "ckeditor/plugins"
libraries[ckeditor_xml][directory_name] = "xml"

libraries[d3][destination] = "libraries"
libraries[d3][directory_name] = "d3"
libraries[d3][download][type] = "get"
libraries[d3][download][url] = "https://github.com/d3/d3/releases/download/v4.4.0/d3.zip"

libraries[pheanstalk][destination] = "libraries"
libraries[pheanstalk][directory_name] = "pheanstalk"
libraries[pheanstalk][download][type] = "get"
libraries[pheanstalk][download][url] = "https://github.com/pda/pheanstalk/archive/v2.1.1.tar.gz"

libraries[leaflet][destination] = "libraries"
libraries[leaflet][directory_name] = "leaflet"
libraries[leaflet][download][type] = "get"
libraries[leaflet][download][url] = "https://github.com/Leaflet/Leaflet/archive/refs/tags/v0.7.7.zip"
libraries[leaflet][download][subtree] = "Leaflet-0.7.7/dist"

; Required by codefilter_prism
libraries[prism][type] = "libraries"
libraries[prism][download][type] = "get"
libraries[prism][download][url] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/get/1af9ec7f8967.zip"
libraries[prism][download][subtree] = "drupalorg-infrastructure-drupal.org-sites-common-1af9ec7f8967/prism"

; Required by asciidoc_display
libraries[markitup][download][type] = "get"
libraries[markitup][download][url] = "https://github.com/markitup/1.x/archive/1.1.14.zip"

libraries[asciidoctor_images][download][type] = "get"
libraries[asciidoctor_images][download][url] = "https://github.com/lordofthejars/asciidoctor-markitup/archive/b29a6185d40d40c04dbed6e3792a312e3de5e983.zip"
libraries[asciidoctor_images][download][subtree] = "asciidoctor-markitup-b29a6185d40d40c04dbed6e3792a312e3de5e983/markitup/sets/asciidoc/images"

libraries[asciidoctor][download][type] = "get"
libraries[asciidoctor][download][url] = "https://github.com/asciidoctor/asciidoctor.js/archive/v1.5.4.zip"
libraries[asciidoctor][download][subtree] = "asciidoctor.js-1.5.4/dist"
libraries[asciidoctor][directory_name] = "asciidoctor/dist"

; Required by advagg.
libraries[loadcss][download][type] = "get"
libraries[loadcss][download][url] = "https://github.com/filamentgroup/loadCSS/archive/v3.1.0.zip"
libraries[loadcss][directory_name] = "loadCSS"


; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"
